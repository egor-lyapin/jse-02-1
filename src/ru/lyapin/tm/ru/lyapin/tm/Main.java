package ru.lyapin.tm;

import ru.lyapin.tm.constant.TerminConst;

import java.util.Arrays;

public class Main {

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        switch (param){
            case TerminConst.VERSION: displayVersion();
            case TerminConst.ABOUT: displayAbout();
            case TerminConst.HELP: displayHelp();
        }
    }

    private static void displayWelcome() {
        System.out.println("*** Welcome to TM ***");
    }

    private static void displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of command.");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Egor Lyapin");
        System.out.println("lyapin_ea2@nlmk.com");
        System.exit(0);
    }
}