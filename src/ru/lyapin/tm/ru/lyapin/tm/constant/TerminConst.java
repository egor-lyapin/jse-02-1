package ru.lyapin.tm.constant;

public final class TerminConst {
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
}
